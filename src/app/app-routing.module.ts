import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastroComponent } from './pages/cadastro/cadastro.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { SobreComponent } from './pages/sobre/sobre.component';


const routes: Routes = [
  {
    path:"",
    component:HomeComponent,
    title:"Home"   
  },
  {
    path:"cadastro",
    component:CadastroComponent,
    title:"Cadastro"
  },
  {
    path:"login",
    component:LoginComponent,
    title:"Login"
  },
  {
    path:"sobre",
    component:SobreComponent,
    title:"Sobre"
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
